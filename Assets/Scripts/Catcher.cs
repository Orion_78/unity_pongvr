﻿using UnityEngine;
using System.Collections;

public class Catcher : MonoBehaviour
{
    [SerializeField]
    private GameObject opponent;

    [SerializeField]
    private Transform targetPosition;

    private Rigidbody _rigidbody;

    Vector3 _previousPos;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _previousPos = transform.position;
    }
    
    [HideInInspector]
    public float maxVelocity = 30.0f;
    float clampSpeed = 20;
    
    float maxAngularVelocity = 10.0f;
    float clampAngularSpeed = 20;

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(targetPosition.position, transform.position);
        Vector3 dir = (targetPosition.transform.position - transform.position).normalized;

        _rigidbody.velocity = dir * Mathf.Clamp( (maxVelocity), 0, dist* clampSpeed);

        /*
        Vector3 difference = transform.eulerAngles - targetPosition.transform.eulerAngles;
        Vector3 velocity = difference / Time.fixedDeltaTime;
        _rigidbody.angularVelocity = velocity; ;
        */
        transform.LookAt(opponent.transform);
            
    }

    /*void LateUpdate()
    {
        transform.position = targetPosition.position;
    }*/

    void OnCollisionEnter(Collision collision)
    {
        if (!collision.collider.GetComponent<Rigidbody>())
            return;

        Vector3 dir = (opponent.transform.position - transform.position).normalized;
        collision.collider.GetComponent<Rigidbody>().AddForce(dir * 50);
    }
}
    
