﻿using UnityEngine;
using UnityEngine.Networking;

public class SyncMovement : NetworkBehaviour {


    [SyncVar]
    Vector3 serverPosition;
    [SyncVar]
    Vector3 serverVelocity;
    [SyncVar]
    Vector3 serverAngularVelocity;

    Rigidbody _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (isServer)
        {
            serverPosition = transform.position;
            serverVelocity = _rigidbody.velocity;
            serverAngularVelocity = _rigidbody.angularVelocity;
        }


        transform.position = serverPosition;// Vector3.Lerp(transform.position, , 0.8f);
        _rigidbody.velocity = serverVelocity;
        _rigidbody.angularVelocity = serverAngularVelocity;
    }

    

}
