﻿using UnityEngine;
using System.Collections;

public class AudioPlay : MonoBehaviour {

    public AudioClip music;

    void Start()
    {
        AudioManager.Instance.PlayMusic(music);
    }
}
