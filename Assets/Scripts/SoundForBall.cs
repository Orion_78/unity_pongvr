﻿using UnityEngine;
using System.Collections;

public class SoundForBall : MonoBehaviour {
    [SerializeField]
    AudioClip[] _audioClip;

    [SerializeField]
    float _volume = 1;

    void OnCollisionEnter(Collision collision)
    {
        if (_audioClip.Length == 0)
            return;

        if (collision.gameObject.GetComponent<Ball>())
        AudioManager.Instance.PlaySoundAtPoint(
            _audioClip[Random.Range(0, _audioClip.Length)],
            collision.contacts[0].point, _volume);
    }
    }
