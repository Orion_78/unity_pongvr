﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour
{

    float speedAdded = 2;
    float maxMagnitude = 5;
    void OnCollisionEnter(Collision collision)
    {
        Vector3 v = collision.collider.GetComponent<Rigidbody>().velocity;

        if (Mathf.Abs(v.z) < maxMagnitude)
            v.z += v.z > 0 ? speedAdded : -speedAdded;
      //  collision.collider.GetComponent<Rigidbody>().velocity = v;
    }
}
