﻿using UnityEngine;
using System;
using UnityEngine.Networking;

public class Unit_ID : NetworkBehaviour
{
    [SerializeField]
    private bool _isLayerModified = true;

    public static int NeutralSide = -1;
    
    [SyncVar]
    [SerializeField]
    private int PlayerIndex;

    [HideInInspector]
    [SyncVar]
    private string my_uniqueID;
    private Transform myTransform;

    public bool IsReady()
    {
        return my_uniqueID != "";
    }

    public int GetPlayerIndex()
    {
        return PlayerIndex;
    }

    public int GetPlayerLayer()
    {
        return PlayerIndex + 10;
    }

    public void SetPlayerIndex(int number)
    {
        PlayerIndex = number;
    }

    public void SetMyUniqueID(string id)
    {
        my_uniqueID = id;
    }

    // Use this for initialization
    void Start()
    {
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        // Set Identity
        myTransform.name = my_uniqueID;
    }

    void LateUpdate()
    {
        // Change Layer
        if (PlayerIndex == NeutralSide)
        {
            if (_isLayerModified)
                gameObject.layer = GetPlayerLayer();
        }
        else
        {
            if (_isLayerModified)
                gameObject.layer = GetPlayerLayer();
        }
    }
}
