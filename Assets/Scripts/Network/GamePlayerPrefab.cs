﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;


public class GamePlayerPrefab : NetworkBehaviour{
        
	
	// Update is called once per frame
	void Update () {
        if (isLocalPlayer && !isServer)
        {
            if (Outils.allLobbyPlayer.Count < 2 ||
            Outils.FindLobbyIndex(Outils.myLobbyPlayer) == 0)
            {
                CmdUpdatePos(PlayerPosition.CatcherA.transform.position, 0);
            }
            else
            {
                CmdUpdatePos(PlayerPosition.CatcherB.transform.position, 1);
            }
            
        }
    }

    [Command]
    void CmdUpdatePos(Vector3 targetPos, int playerID)
    {
        if (playerID == 0)
        {
            PlayerPosition.CatcherA.transform.position = targetPos;
        }
        else
        {
            PlayerPosition.CatcherB.transform.position = targetPos;
        }
       
    }
}
