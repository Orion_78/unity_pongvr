﻿using UnityEngine;
using System.Collections;

public class PlayerPosition : MonoBehaviour {
    [SerializeField]
    GameObject _opponent, _catcherA, _catcherB;
    [SerializeField]
    public static GameObject CatcherA, CatcherB;

	// Use this for initialization
	void Awake () {
        CatcherA = _catcherA;
        CatcherB = _catcherB;

        if (Outils.allLobbyPlayer.Count < 2 ||
            Outils.FindLobbyIndex(Outils.myLobbyPlayer) == 0)
        {
           
        }
        else
        {
            _catcherA.transform.parent = transform.parent;

            transform.parent = _opponent.transform;
            
            transform.localPosition = Vector3.zero;
            _catcherB.transform.parent = transform.GetChild(0);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
