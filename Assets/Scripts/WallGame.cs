﻿using UnityEngine;
using System.Collections;

public class WallGame : MonoBehaviour {

    [SerializeField]
    GameManager _gameManager;

    [SerializeField]
    int player;
    void OnCollisionEnter(Collision collision)
    {
        _gameManager.MarkScore(player);
        AudioManager.Instance.PlaySound(AudioManager.Instance.Goal, 0.5f);
    }
}
