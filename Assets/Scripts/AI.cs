﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour {

    [SerializeField]
    GameObject _ball;
    [SerializeField]
    Catcher _catcher;

    void Start()
    {
        if (Outils.allLobbyPlayer.Count > 1)
        {
            enabled = false;
        }
        else
        {
            _catcher.maxVelocity = 1;
        }
    }

    void Update()
    {
        transform.LookAt(_ball.transform);
    }
}
