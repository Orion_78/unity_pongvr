﻿using UnityEngine;
using System.Collections.Generic;

public class Outils : MonoBehaviour
{

    public static List<Prototype.NetworkLobby.LobbyPlayer> allLobbyPlayer = new List<Prototype.NetworkLobby.LobbyPlayer>();
    public static Prototype.NetworkLobby.LobbyPlayer myLobbyPlayer;

    public static int GetMyPlayerID()
    {
        return FindLobbyIndex(myLobbyPlayer);
    }

    public static int FindLobbyIndex(Prototype.NetworkLobby.LobbyPlayer player)
    {
        int i = 0;
        foreach (var c in allLobbyPlayer)
        {
            if (c == player)
            {
                return i;
            }
            i++;
        }

        return -1;
    }
}
