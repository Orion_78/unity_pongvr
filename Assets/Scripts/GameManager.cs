﻿using UnityEngine;
using System;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour {
    [HideInInspector]
    [SyncVar]
    public int _scoreA = 0;
    [HideInInspector]
    [SyncVar]
    public int _scoreB = 0;

    public Action Goal = delegate { };
    
     
    public void MarkScore(int player)
    {
        if (!isServer)
            return;
        if (player == 0)
            _scoreA++;
        else
            _scoreB++;
        
        RpcGoal();
    }

    [ClientRpc]
    void RpcGoal()
    {
        Goal();
    }
}
