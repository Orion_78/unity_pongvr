﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    bool BallLaunched;

    Rigidbody _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

	void OnEnable()
    {
        BallLaunched = false;
    }

    float minSpeed = 2;
    void Update()
    {
        if (!BallLaunched && _rigidbody.velocity.magnitude > 0)
        {
            BallLaunched = true;
        }

        /*if (BallLaunched && _rigidbody.velocity.magnitude < minSpeed)
        {
            _rigidbody.velocity = _rigidbody.velocity * minSpeed;
        }*/

        if (BallLaunched && Mathf.Abs(_rigidbody.velocity.z) < minSpeed)
        {
            Vector3 v = _rigidbody.velocity;

            float distA = Vector3.Distance(transform.position, PlayerPosition.CatcherA.transform.position);

            float distB = Vector3.Distance(transform.position, PlayerPosition.CatcherB.transform.position);

            Debug.Log(distA > distB);
            v.z += distA < distB ? minSpeed : -minSpeed;
            _rigidbody.velocity = v;
        }
    }
}
