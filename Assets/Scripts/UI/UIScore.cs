﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIScore : MonoBehaviour {

    [SerializeField]
    Text _scoreA;
    [SerializeField]
    Text _scoreB;

    [SerializeField]
    GameManager _gameManager;

    void Start()
    {
        UpdateScore();
        _gameManager.Goal += UpdateScore;
        
    }

    void UpdateScore()
    {
        _scoreA.text = "" + _gameManager._scoreA;
        _scoreB.text = "" + _gameManager._scoreB;
    }
}
